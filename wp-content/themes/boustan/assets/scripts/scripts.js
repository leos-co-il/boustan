(function($) {
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	$( document ).ready(function() {
		$('#MainNav .menu-item-has-children').hover(
			function() {
				$(this).children('.collapse-menu').toggleClass('show-menu');
			});
		$('.pop-trigger').click(function () {
			$('.pop-form').addClass('show-popup');
			$('.float-form').addClass('show-float-form');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-form').click(function () {
			$('.pop-form').removeClass('show-popup');
			$('.float-form').removeClass('show-float-form');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.same-posts-slider').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			arrows: true,
			dots: false,
			rtl: true,
			responsive: [
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 650,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$('.cats-slider').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			arrows: true,
			dots: false,
			rtl: true,
			responsive: [
				{
					breakpoint: 1040,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 769,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$('.team-slider').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			rtl: true,
			infinite: true,
			centerMode: true,
			responsive: [
				{
					breakpoint: 992,
					settings: {
						centerPadding: '0',
						slidesToShow: 2,
						centerMode: false,
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 1,
						centerMode: false,
					}
				}
			]
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-video').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().addClass('active-faq');
			show.parent().children('.question-title').children('.arrow-bottom').addClass('arrow-top');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().removeClass('active-faq');
			collapsed.parent().children('.question-title').children('.arrow-bottom').removeClass('arrow-top');
		});
	});
	//Load more items
	var sizeLi = $('#success-block .teacher-col').size();
	var x = 6;
	$('#success-block .teacher-col:lt('+x+')').addClass('show');
	$('.load-more-teachers').click(function () {
		x = (x + 3 <= sizeLi) ? x + 3 : sizeLi;
		$('#success-block .teacher-col:lt('+x+')').addClass('show');
		if (x === sizeLi) {
			$('.load-more-teachers').addClass('hide');
		}
	});
	$('.load-more-posts').click(function() {
		var termID = $(this).data('term');
		var params = $('.take-json').html();
		var postType = $(this).data('type');
		var taxType = $(this).data('tax-type');

		var ids = '';
		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				postType: postType,
				termID: termID,
				ids: ids,
				taxType: taxType,
				params: params,
				action: 'get_more_function',
			},
			success: function (data) {
				console.log(data);
				if (!data.html) {
					$('.load-more-posts').addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});

})( jQuery );
