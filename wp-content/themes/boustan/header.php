<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header>
    <div class="container-fluid">
        <div class="row justify-content-center">
			<div class="col-xl-11 col-12">
				<div class="row justify-content-between">
					<div class="col-auto">
						<nav id="MainNav" class="top-nav">
							<div id="MobNavBtn">
								<span></span>
								<span></span>
								<span></span>
							</div>
							<?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
						</nav>
					</div>
					<?php if ($tel = opt('tel')) : ?>
						<div class="col-auto">
							<a href="tel:<?= $tel; ?>" class="header-tel d-flex justify-content-center align-items-center">
								<img src="<?= ICONS ?>header-tel.png" alt="tel">
								<span class="tel-number">
									<?= $tel; ?>
								</span>
							</a>

						</div>
					<?php endif; ?>
				</div>
			</div>
        </div>
    </div>
</header>
<div class="triggers-wrap">
	<div class="pop-trigger">
		צרו קשר
	</div>
	<?php if ($whatsapp = opt('whatsapp')) : ?>
		<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" target="_blank" class="social-link">
			<img src="<?= ICONS ?>whatsapp.png" alt="whatsapp">
		</a>
	<?php endif;
	if ($facebook = opt('facebook')) : ?>
	<a href="<?= $facebook; ?>" target="_blank" class="social-link">
		<img src="<?= ICONS ?>facebook.png" alt="facebook">
	</a>
	<?php endif; ?>
</div>
<section class="pop-form">
	<div class="container-fluid">
		<div class="row justify-content-center align-items-center">
			<div class="col-xl-6 col-auto form-col-wrap">
				<div class="float-form">
					<span class="close-form">
						X
					</span>
					<div class="form-col">
						<?php if ($logo = opt('logo')) : ?>
							<a class="logo-form" href="/">
								<img src="<?= $logo['url']; ?>" alt="logo">
							</a>
						<?php endif;
						if ($f_title = opt('pop_form_title')) : ?>
							<h2 class="pop-form-title"><?= $f_title; ?></h2>
						<?php endif;
						if ($f_text = opt('pop_form_subtitle')) : ?>
							<h3 class="pop-form-subtitle"><?= $f_text; ?></h3>
						<?php endif;
						getForm('8'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
