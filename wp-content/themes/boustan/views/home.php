<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<section class="home-main-block" <?php if (has_post_thumbnail()) : ?>
	style="background-image: url('<?= postThumb(); ?>')"
<?php endif; ?>>
	<div class="container">
		<div class="row">
			<div class="col-12 d-flex flex-column justify-content-center align-items-center wow zoomIn" data-wow-delay="0.2s">
				<?php if ($logo = opt('logo_white')) : ?>
					<a href="/" class="logo main-logo">
						<img src="<?= $logo['url'] ?>" alt="logo">
					</a>
				<?php endif;
				if ($fields['home_title']) : ?>
					<h2 class="main-home-title"><?= $fields['home_title']; ?></h2>
				<?php endif; ?>
				<a class="base-link home-block-link main-link" href="<?= ($fields['home_link_main'] && isset($fields['home_link_main']['url'])) ?
						$fields['home_link_main']['url'] : get_page_link(getPageByTemplate('views/contact.php')); ?>">
					<?= ($fields['home_link_main'] && isset($fields['home_link_main']['title'])) ?
							$fields['home_link_main']['title'] : 'צרו איתי קשר'; ?>
				</a>
			</div>
		</div>
	</div>
</section>
<?php if ($fields['home_about_text'] || $fields['home_about_img']) : ?>
	<section class="home-about">
		<div class="container">
			<div class="row align-items-center">
				<?php if ($fields['home_about_text']) : ?>
				<div class="col py-4 d-flex flex-column align-items-start">
					<div class="base-output">
						<?= $fields['home_about_text']; ?>
					</div>
					<?php if ($fields['home_about_link']) : ?>
						<a class="base-link home-about-link" href="<?= $fields['home_about_link']['url']; ?>">
							<?= (isset($fields['home_about_link']['title']) && $fields['home_about_link']['title']) ?
									$fields['home_about_link']['title'] : 'להמשך קריאה'; ?>
						</a>
					<?php endif; ?>
				</div>
				<?php endif;
				if ($fields['home_about_img']) : ?>
					<div class="col-lg-6 about-img-col">
						<img src="<?= $fields['home_about_img']['url']; ?>" alt="about-img">
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['home_cats_text'] || $fields['home_cats']) : ?>
	<section class="cats-slider-block">
		<?php if ($fields['home_cats_text']) {
			get_template_part('views/partials/content', 'block_text', [
					'text' => $fields['home_cats_text'],
			]);
		} if ($fields['home_cats']) : ?>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="cats-slider" dir="rtl">
							<?php foreach ($fields['home_cats'] as $curr_cat) :
								$link = get_term_link($curr_cat->term_id); ?>
								<div class="cat-slide">
									<div class="post-card-column cat-card">
										<a class="post-img cat-image" href="<?= $link; ?>"
												<?php if ($img = get_field('cat_img', $curr_cat)) : ?>
													style="background-image: url('<?= $img['url']; ?>')"
												<?php endif; ?>>
										</a>
										<div class="post-card-content cat-card-content">
											<a class="mid-title cat-card-title" href="<?= $link; ?>"><?= $curr_cat->name; ?></a>
											<p class="cat-text-item">
												<?= text_preview(category_description($curr_cat), 20); ?>
											</p>
										</div>
										<a class="base-link" href="<?= $link; ?>">
											מעבר לפעילות
										</a>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</section>
<?php endif;
if ($fields['home_benefit']) : ?>
	<section class="benefits-section">
		<div class="container">
			<div class="row justify-content-center align-items-center">
				<div class="col">
					<div class="benefits-col">
						<?php foreach ($fields['home_benefit'] as $x => $benefit) : ?>
							<div class="benefit-item wow fadeInRight" data-wow-delay="0.<?= $x + 1; ?>s">
								<h3 class="ben-title"><?= $benefit['benefit_text']; ?></h3>
								<span class="benefit-number"><?= $x + 1; ?></span>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="col-auto circle-col">
					<div class="circle-ben">
						<?php if ($logo = opt('logo')) : ?>
							<div class="logo-ben">
								<img src="<?= $logo['url']; ?>" alt="logo">
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="col ben-img-col">
					<?php if($fields['home_benefits_img']) : ?>
						<div class="ben-img">
							<img src="<?= $fields['home_benefits_img']['url']; ?>" alt="dancing-girl">
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'form');
if ($fields['home_team_text'] || $fields['home_team_item'] || $fields['home_team_link']) : ?>
	<section class="home-team-block">
		<?php if ($fields['home_team_text']) {
			get_template_part('views/partials/content', 'block_text', [
					'text' => $fields['home_team_text'],
			]);
		} if ($fields['home_team_item'] || $fields['home_team_link']) : ?>
		<div class="container-fluid mt-2 arrows-slider-violet">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-12">
					<?php if ($fields['home_team_item']) : ?>
					<div class="team-slider" dir="rtl">
						<?php foreach ($fields['home_team_item'] as $teacher) : ?>
							<div class="team-slide">
								<div class="home-team-card">
									<div class="home-team-img"<?php if ($teacher['team_img']) : ?>
										style="background-image: url('<?= $teacher['team_img']['url']; ?>')"
									<?php endif; ?>>
										<span class="team-overlay"></span>
									</div>
									<?php if ($teacher['team_text']) : ?>
										<div class="team-content">
											<div class="team-item-output">
												<?= $teacher['team_text']; ?>
											</div>
										</div>
									<?php endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
					<?php endif; ?>
				</div>
			</div>
			<?php if ($fields['home_team_link']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<a class="base-link home-block-link px-3 mt-2" href="<?= $fields['home_team_link']['url']; ?>">
							<?= (isset($fields['home_team_link']['title']) && $fields['home_team_link']['title']) ?
									$fields['home_team_link']['title'] : 'בואו להכיר כל הצוות'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
		<?php endif; ?>
	</section>
<?php endif;
if ($fields['home_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['home_slider_seo'],
		'img' => $fields['home_slider_img'],
		]);
}
if ($fields['home_video_slider']) : ?>
	<section class="home-video-block">
		<div class="container-fluid">
			<div class="row justify-content-center arrows-slider-violet">
				<?php if ($fields['home_video_title']) : ?>
					<div class="col-sm-11 col-12">
						<h2 class="base-title mr-sm-4 mr-0"><?= $fields['home_video_title']; ?></h2>
					</div>
				<?php endif; ?>
				<div class="col-11">
					<div class="same-posts-slider" dir="rtl">
						<?php foreach ($fields['home_video_slider'] as $video) : ?>
							<div class="video-slide">
								<div class="video-item" style="background-image: url('<?= getYoutubeThumb($video['video_link']); ?>')">
									<span class="play-video" data-video="<?= getYoutubeId($video['video_link'])?>">
										<img src="<?= ICONS ?>play-button.png" alt="play-video">
									</span>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="video-modal">
		<div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
			 aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-body" id="iframe-wrapper"></div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="close-icon">×</span>
					</button>
				</div>
			</div>
		</div>
	</div>
<?php endif;
if ($fields['faq_item']) : ?>
	<div class="faq-teachers faq-home mb-5">
		<?php get_template_part('views/partials/content', 'faq',
				[
						'block_title' => $fields['faq_title'],
						'faq' => $fields['faq_item'],
						'img' => $fields['faq_img'],
				]); ?>
	</div>
<?php endif;
get_template_part('views/partials/repeat', 'form');
if ($fields['home_posts'] || $fields['home_posts_link'] || $fields['home_posts_title']) : ?>
	<section class="home-posts-block">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<?php if ($fields['home_posts_title']) : ?>
					<div class="col-sm-11 col-12">
						<h2 class="base-title mb-4"><?= $fields['home_posts_title']; ?></h2>
					</div>
				<?php endif;
				if ($fields['home_posts']) : ?>
					<div class="col-11 ">
						<?php foreach ($fields['home_posts'] as $y => $post_item) : $post_link = get_the_permalink($post_item); ?>
							<div class="row justify-content-center align-items-stretch post-row wow fadeInLeft"
								 data-wow-delay="0.<?= $y * 2; ?>s">
								<?php if (has_post_thumbnail()) : ?>
									<div class="col-md-6 home-post-col"
										 style="background-image: url('<?= postThumb($post_item)?>')">
									</div>
								<?php endif; ?>
								<div class="col-md-6 shadow-img-post">
									<div class="home-post-content">
										<a class="base-text mb-3 font-weight-bold" href="<?= $post_link; ?>">
											<?= $post_item->post_title; ?>
										</a>
										<p class="base-text mb-4">
											<?= text_preview($post_item->post_content, 30); ?>
										</p>
										<a href="<?= $post_link; ?>" class="base-link home-block-link home-post-link align-self-end">
											להמשך קריאה
										</a>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<?php if ($fields['home_posts_link']) : ?>
				<div class="row justify-content-center mt-4">
					<div class="col-11">
						<div class="row justify-content-end">
							<div class="col-auto">
								<a href="<?= $fields['home_posts_link']['url']; ?>" class="base-link home-block-link">
									<?= (isset($fields['home_posts_link']['title']) && $fields['home_posts_link']['title']) ?
											$fields['home_posts_link']['title'] : 'לכל המאמרים שלנו'; ?>
								</a>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) : ?>
	<section class="slider-base-wrap arrows-slider-black transparent-slider home-slider-page">
		<div class="purple-circle"></div>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-md-11 col-12">
					<div class="row justify-content-center align-items-center">
						<div class="col-lg-6 col-md-7 col-sm-11 col-12 slider-wrap-col">
							<div class="slider-text-wrap">
								<div class="base-slider" dir="rtl">
									<?php foreach ($fields['single_slider_seo'] as $content) : ?>
										<div>
											<div class="base-output white-output"><?= $content['content']; ?></div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
						<?php if ($fields['slider_img']) : ?>
							<div class="col-lg-6 col-md-5 col-sm-11 col-12 position-relative home-img-col">
								<img src="<?= $fields['slider_img']['url']; ?>" class="slider-home-img">
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
get_footer(); ?>
