<?php
/*
Template Name: מאמרים
*/
get_header();
$fields = get_fields();
$post_type = (isset($fields['type']) && ($fields['type'] === 'class')) ? 'class' : 'post';
$type_cat = ($post_type === 'class') ? 'class_cat' : 'category';
$posts = get_posts([
	'numberposts' => 6,
	'post_type' => $post_type,
]);
$posts_all = get_posts([
	'numberposts' => -1,
	'post_type' => $post_type,
]);
$cats = get_terms([
		'taxonomy' => $type_cat,
		'hide_empty' => false,
]);
get_template_part('views/partials/content', 'top_page', [
		'title' => get_the_title(),
]);?>
<article class="page-body">
	<?php get_template_part('views/partials/content', 'block_text', [
		'title' => get_the_title(),
		'text' => get_the_content(),
	]); ?>
	<div class="body-output">
		<?php if ($cats) : ?>
			<div class="container mb-4">
				<div class="row justify-content-center">
					<?php foreach ($cats as $cat) : ?>
						<div class="col-auto mb-2">
							<a class="cat-link" href="<?= get_term_link($cat); ?>">
								<?= $cat->name; ?>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif;
		if ($posts) : ?>
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-sm-11 col-12">
						<div class="row justify-content-center align-items-stretch put-here-posts">
							<?php foreach ($posts as $post) {
								get_template_part('views/partials/card', 'post_ajax', [
										'post' => $post,
								]);
							} ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php if (count($posts_all) > 6) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="load-more-link load-more-posts" data-type="<?= $post_type; ?>"
						 data-tax-type="<?= ($post_type === 'class') ? 'class_cat': 'category; '?>">
						<?= ($post_type === 'class') ? 'לעוד חוגים נהדרים' : 'טען עוד מאמרים'; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php get_template_part('views/partials/repeat', 'form');
if ($fields['single_slider_seo']) : ?>
	<div class="transparent-slider">
		<?php get_template_part('views/partials/content', 'slider', [
				'content' => $fields['single_slider_seo'],
				'img' => $fields['slider_img'],
		]); ?>
	</div>
<?php endif;
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
			[
					'block_title' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
					'img' => $fields['faq_img'],
			]);
}
get_footer(); ?>

