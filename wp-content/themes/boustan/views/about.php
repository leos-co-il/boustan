<?php
/*
Template Name: אודות
*/
get_header();
$fields = get_fields();
get_template_part('views/partials/content', 'top_page', [
		'title' => get_the_title(),
]);?>
<article class="page-body mt-4">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-md-11 col-12">
				<div class="row align-items-start">
					<div class="col">
						<div class="base-output">
							<?php the_content(); ?>
						</div>
					</div>
					<?php if (has_post_thumbnail()) : ?>
						<div class="col-lg-6 about-img-col">
							<img src="<?= postThumb(); ?>" alt="about-img">
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php get_template_part('views/partials/content', 'block_text', [
			'text' => $fields['about_cats_text'],
	]);
	if ($fields['about_cats_chosen']) : ?>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-sm-11 col-12">
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($fields['about_cats_chosen'] as $x => $cat) {
							get_template_part('views/partials/card', 'category', [
									'cat' => $cat,
							]);
						} ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php get_template_part('views/partials/repeat', 'form');
if ($fields['faq_item']) : ?>
	<div class="faq-teachers">
		<?php get_template_part('views/partials/content', 'faq',
				[
						'block_title' => $fields['faq_title'],
						'faq' => $fields['faq_item'],
						'img' => $fields['faq_img'],
				]); ?>
	</div>
<?php endif;
get_footer(); ?>

