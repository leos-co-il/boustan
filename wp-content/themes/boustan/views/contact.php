<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();
get_template_part('views/partials/content', 'top_page', [
	'title' => get_the_title(),
]); ?>
<article class="page-body contact-page">
	<div class="container position-relative">
		<?php if (has_post_thumbnail()) : ?>
			<div class="contact-image">
				<img src="<?= postThumb(); ?>" alt="dancing-girl">
			</div>
		<?php endif; ?>
		<div class="row justify-content-center contact-content-row">
			<div class="col-xl-9 col-lg-10 col-12">
				<div class="base-output contact-page-output">
					<?php the_content(); ?>
				</div>
				<div class="form wow zoomIn">
					<?php getForm('10'); ?>
				</div>
			</div>
			<div class="col-xl-9 col-lg-10 col-12">
				<?php get_template_part('views/partials/repeat', 'contact_list'); ?>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
