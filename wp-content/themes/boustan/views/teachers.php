<?php
/*
Template Name: צוות
*/
get_header();
$fields = get_fields();
get_template_part('views/partials/content', 'top_page', [
	'title' => get_the_title(),
]);?>
<article class="page-body mt-4">
	<?php get_template_part('views/partials/content', 'block_text', [
		'text' => get_the_content(),
	]);
	if ($fields['teachers']) : ?>
		<div class="body-output teachers-block">
			<div class="container">
				<div class="row justify-content-center align-items-stretch" id="success-block">
					<?php foreach ($fields['teachers'] as $i => $teacher) : ?>
						<div class="col-lg-4 col-md-6 col-12 teacher-col">
							<div class="teacher-card" <?php if ($teacher['teacher_img']) : ?>
								style="background-image: url('<?= $teacher['teacher_img']['url'];  ?>')"
							<?php endif; ?>>
								<div class="base-teach-overlay"></div>
								<?php if ($teacher['teacher_text']) : ?>
									<div class="about-teacher-wrap">
										<p class="about-teacher-text"><?= $teacher['teacher_text']; ?></p>
									</div>
								<?php endif; ?>
								<?php if ($teacher['teacher_name']) : ?>
									<div class="teacher-name-wrap">
										<h3 class="teacher-name"><?= $teacher['teacher_name']; ?></h3>
									</div>
								<?php endif; ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<?php if (count($fields['teachers']) > 6) : ?>
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-auto">
							<div class="load-more-link load-more-teachers">
								לעוד מורים נהדרים
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	<?php endif; ?>
</article>
<?php get_template_part('views/partials/repeat', 'form');
if ($fields['faq_item']) : ?>
	<div class="faq-teachers">
		<?php get_template_part('views/partials/content', 'faq',
		[
			'block_title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
			'img' => $fields['faq_img'],
		]); ?>
	</div>
<?php endif;
get_footer(); ?>

