<?php if (isset($args['content']) && $args['content']) : ?>
	<section class="slider-base-wrap arrows-slider-black">
		<?php if (isset($args['img']) && $args['img']) : ?>
			<div class="col-lg-6 slider-image-wrap">
				<img src="<?= $args['img']['url']; ?>" class="slider-image">
			</div>
		<?php endif; ?>
		<div class="black-slider">
			<div class="container-fluid">
				<div class="row justify-content-center align-items-center">
					<?php if (isset($args['img']) && $args['img']) : ?>
						<div class="col-lg-5 d-flex justify-content-center align-items-center">
						</div>
					<?php endif; ?>
					<div class="col-lg-7 col-11 slider-wrap-col">
						<div class="slider-text-wrap">
							<div class="base-slider" dir="rtl">
								<?php foreach ($args['content'] as $content) : ?>
									<div>
										<div class="base-output white-output"><?= $content['content']; ?></div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
