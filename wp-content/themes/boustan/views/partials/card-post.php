<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']);
	$currentType = get_post_type($args['post']->ID);?>
	<div class="post-card-column">
		<a class="post-img" href="<?= $link; ?>"
			<?php if (has_post_thumbnail($args['post'])) : ?>
				style="background-image: url('<?= postThumb($args['post']); ?>')"
			<?php endif; ?>>
			<span class="post-overlay"></span>
		</a>
		<div class="post-card-content">
			<a class="mid-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
			<p class="base-text mb-2">
				<?= text_preview($args['post']->post_content, 20); ?>
			</p>
		</div>
		<a class="base-link" href="<?= $link; ?>">
			<?= ($currentType === 'class') ? 'מעבר לפעילות' : 'לקריאה'; ?>
		</a>
	</div>
<?php endif; ?>
