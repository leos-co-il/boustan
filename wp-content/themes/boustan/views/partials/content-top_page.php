<section class="page-top">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto d-flex align-items-end justify-content-center flex-wrap">
				<?php if(isset($args['title']) && $args['title']) : ?>
					<h1 class="form-title">
						<?= $args['title']; ?>
					</h1>
				<?php endif;
				if ($logo = opt('logo_simple')) : ?>
					<a href="/" class="logo top-logo">
						<img src="<?= $logo['url'] ?>" alt="logo">
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<?php if ( function_exists('yoast_breadcrumb') ) : ?>
	<div class="breads-container">
		<div class="container-fluid">
			<div class="row justify-content-center bread-row bread-row-body">
				<div class="col-lg-11 col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
