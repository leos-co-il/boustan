<?php if (isset($args['faq']) && $args['faq']) :
	$img_link = IMG.'faq-img.png'; ?>
	<div class="faq" style="background-image: url('<?= $args['img'] ? $args['img']['url'] : $img_link;?>')">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-11">
					<div class="row">
						<div class="col-lg-7 col-12">
							<h2 class="base-title mb-4">
								<?= (isset($args['title']) && $args['title']) ? $args['title'] : 'אנחנו נענה על כל שאלה!'; ?>
							</h2>
							<div id="accordion">
								<?php foreach ($args['faq'] as $num => $item) : ?>
									<div class="card question-card wow fadeInUp" data-wow-delay="0.<?= $num + $i = 1; ?>s" <?php $i++; ?>>
										<div class="question-header" id="heading_<?= $num; ?>">
											<span class="q-icon">?</span>
											<button class="btn question-title" data-toggle="collapse"
													data-target="#faqChild<?= $num; ?>"
													aria-expanded="false" aria-controls="collapseOne">
												<?= $item['faq_question']; ?>
												<img class="arrow-bottom" src="<?= ICONS ?>faq-bottom.png" alt="arrow">
											</button>
											<div id="faqChild<?= $num; ?>" class="collapse faq-item"
												 aria-labelledby="heading_<?= $num; ?>" data-parent="#accordion">
												<div class="answer-body base-output">
													<?= $item['faq_answer']; ?>
												</div>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
						<div class="col-lg-5 col-12 faq-main-img">
							<img src="<?= $args['img'] ? $args['img']['url'] : $img_link; ?>" alt="dancing-queen">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
