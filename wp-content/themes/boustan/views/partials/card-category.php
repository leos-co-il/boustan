<?php if (isset($args['cat']) && $args['cat']) : $link = get_term_link($args['cat']->term_id); ?>
	<div class="col-lg-4 col-sm-6 col-12 post-col">
		<div class="post-card-column">
			<a class="post-img" href="<?= $link; ?>"
				<?php if ($img = get_field('cat_img', $args['cat'])) : ?>
					style="background-image: url('<?= $img['url']; ?>')"
				<?php endif; ?>>
				<span class="post-overlay"></span>
			</a>
			<div class="post-card-content">
				<a class="mid-title" href="<?= $link; ?>"><?= $args['cat']->name; ?></a>
				<p class="base-text mb-2">
					<?= text_preview(category_description($args['cat']), 20); ?>
				</p>
			</div>
			<a class="base-link" href="<?= $link; ?>">
				מעבר לפעילות
			</a>
		</div>
	</div>
<?php endif; ?>
