<section class="repeat-form-block">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<?php if ($rep_title = opt('rep_form_title')) : ?>
					<h2 class="form-title"><?= $rep_title; ?></h2>
				<?php endif;
				if ($rep_text = opt('rep_form_subtitle')) : ?>
					<h3 class="form-subtitle mb-3">
						<?= $rep_text; ?>
					</h3>
				<?php endif; ?>
				<div class="middle-form wow zoomIn">
					<?php getForm('9'); ?>
				</div>
			</div>
		</div>
	</div>
</section>
