<div class="container">
	<div class="row justify-content-center">
		<div class="col-lg-8 col-md-10 col-12 mb-2">
			<?php if (isset($args['title']) && $args['title']) : ?>
				<h2 class="base-title text-center">
					<?= $args['title']; ?>
				</h2>
			<?php endif; ?>
			<?php if (isset($args['text']) && $args['text']) : ?>
				<div class="base-output text-center">
					<?= $args['text']; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
