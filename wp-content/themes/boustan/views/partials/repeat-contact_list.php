<?php $tel = opt('tel');
$tel_2 = opt('tel_2'); ?>
<ul class="contact-list">
	<?php if ($tel || $tel_2) : ?>
		<li class="contact-item col-auto">
			<a href="tel:<?= $tel ? $tel : $tel_2; ?>" class="contact-info-footer">
				<span class="contact-icon-wrap">
					<img src="<?= ICONS ?>header-tel.png">
				</span>
				<span><?= $tel.'|'; ?></span>
				<span><?= $tel_2; ?></span>
			</a>
		</li>
	<?php endif; ?>
	<?php if ($mail = opt('mail')) : ?>
		<li class="contact-item col-auto">
			<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
				<span class="contact-icon-wrap">
					<img src="<?= ICONS ?>foo-mail.png">
				</span>
				<span><?= $mail; ?></span>
			</a>
		</li>
	<?php endif; ?>
	<?php if ($address = opt('address')) : ?>
		<li class="contact-item col-auto">
			<a href="https://waze.com/ul?q=<?= $address; ?>"
			   class="contact-info-footer" target="_blank">
				<span class="contact-icon-wrap">
					<img src="<?= ICONS ?>foo-geo.png">
				</span>
				<span><?= $address; ?></span>
			</a>
		</li>
	<?php endif; ?>
</ul>
