<?php

the_post();
get_header();
$fields = get_fields();

$postId = get_the_ID();
$currentType = get_post_type($postId);
$taxname = '';
$text = '';
switch ($currentType) {
	case 'post':
		$taxname = 'category';
		$text = '<h2>מאמרים נוספים</h2>';
		break;
	case 'class':
		$taxname = 'class_cat';
		$text = '<h2>אהבתם חוג זה? אולי תאהבו גם:</h2>';
		break;
	default:
		$currentType = 'post';
		$taxname = 'category';
}
$type = $currentType === 'class';
$post_terms = wp_get_object_terms($postId, $taxname, ['fields' => 'ids']);
$samePosts = [];
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} else {
	$samePosts = get_posts([
		'posts_per_page' => 4,
		'post_type' => $currentType,
		'post__not_in' => array($postId),
		'tax_query' => [
			[
				'taxonomy' => $taxname,
				'field' => 'term_id',
				'terms' => $post_terms,
			],
		],
	]);
}
if ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 4,
		'orderby' => 'rand',
		'post_type' => $currentType,
		'post__not_in' => array($postId),
	]);
}
get_template_part('views/partials/content', 'top_page', [
	'title' => get_the_title(),
]);?>
<article class="page-body post-body hidden-over">
	<div class="body-output">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="<?= $type ? 'col-xl-10 col-lg-11' : 'col-xl-9 col-lg-10 col-sm-10 col-12'; ?>">
					<div class="row align-items-start">
						<?php if ($type && $cats = get_terms(['taxonomy' => 'class_cat', 'hide_empty' => false,])) : ?>
							<div class="col-xl-2 col-md-3 col-sm-4 mb-4">
								<?php foreach ($cats as $cat) :
									$current_term = in_array($cat->term_id, $post_terms); ?>
									<a class="cat-link cat-link-side <?= $current_term ? 'current-cat' : '';?>"
									   href="<?= get_term_link($cat); ?>">
										<?= $cat->name; ?>
									</a>
									<?php if ($current_term) :
									$curr_posts = get_posts([
											'numberposts' => -1,
											'post_type' => 'class',
											'tax_query' => array(
													array(
															'taxonomy' => 'class_cat',
															'field' => 'term_id',
															'terms' => $cat->term_id,
													)
											)
									]); ?>
										<div class="curr-cat-posts-wrap">
											<?php foreach ($curr_posts as $post_of_cat) : ?>
												<a href="<?php the_permalink($post_of_cat); ?>" class="curr-cat-post">
													<?= $post_of_cat->post_title; ?>
												</a>
											<?php endforeach; ?>
										</div>
									<?php endif; ?>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
						<div class="col content-post-col">
							<div class="base-output post-output">
								<?php the_content(); ?>
							</div>
							<?php if (!$type) : $post_link = get_the_permalink(); ?>
								<div class="socials-share">
									<a href="mailto:?&subject=&body=<?= $post_link; ?>" target="_blank"
									   class="share-link share-mail" role="link">
										<img src="<?= ICONS ?>foo-mail.png" alt="share-mail">
									</a>
									<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
									   class="share-link share-fb">
										<i class="fab fa-facebook-f"></i>
									</a>
									<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $post_link; ?>" target="_blank"
									   class="share-link share-whats" role="link">
										<i class="fab fa-whatsapp"></i>
									</a>
								</div>
							<?php endif; ?>
						</div>
						<div class="<?= $type ? 'col-xl-4 col-12 slider-gallery-arrows arrows-slider-black' : 'col-lg-5'; ?> col-12">
							<?php if (!$type && has_post_thumbnail()) : ?>
								<img src="<?= postThumb(); ?>" alt="post-img" class="w-100 mb-4">
							<?php endif;
							if ($type) : ?>
								<div class="base-slider mb-5" dir="rtl">
									<?php if (has_post_thumbnail()) : ?>
									<div class="slide-gallery">
										<a href="<?= postThumb(); ?>" data-lightbox="gallery-of-class" class="gallery-img"
										   style="background-image: url('<?= postThumb(); ?>')">
										</a>
									</div>
									<?php endif;
									foreach ($fields['class_gallery'] as $img) : ?>
										<div class="slide-gallery">
											<a href="<?= $img['url']; ?>" data-lightbox="gallery-of-class" class="gallery-img"
											style="background-image: url('<?= $img['url']; ?>')">
											</a>
										</div>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
							<div class="post-form-wrap">
								<h3 class="post-form-title <?= $type ? 'form-title-class' : ''; ?>">
									<?= $type ? opt('class_form_title') : opt('post_form_title'); ?>
								</h3>
								<?php if ($type && ($subtitle = opt('class_form_subtitle'))) : ?>
									<p class="post-form-subtitle">
										<?= $subtitle; ?>
									</p>
								<?php endif;
								if ($form_img = opt('post_form_img')) : ?>
									<img src="<?= $form_img['url']; ?>" alt="form-img" class="post-form-img">
								<?php endif;
								getForm('11'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<section class="same-block">
	<?php get_template_part('views/partials/content', 'block_text', [
			'text' => $fields['same_text'] ? $fields['same_text'] : $text,
	]); ?>
	<?php if ($samePosts) : ?>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-sm-11 col-12 slider-post-arrow">
					<div class="same-posts-slider" dir="rtl">
						<?php foreach ($samePosts as $post) : ?>
							<div class="post-slide">
								<?php get_template_part('views/partials/card', 'post', [
										'post' => $post,
								]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</section>
<?php if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
		]);
}
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
		[
			'block_title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
			'img' => $fields['faq_img'],
		]);
}
get_footer(); ?>
