<?php

$tel = opt('tel');
$mail = opt('mail');
$facebook = opt('facebook');
$address = opt('address');
?>
<footer>
	<a id="go-top">
		<img src="<?= ICONS ?>to-top.png">
	</a>
	<div class="footer-main" <?php if ($foo_back = opt('foo_back')) : ?>
		style="background-image: url('<?= $foo_back['url']; ?>')"
	<?php endif; ?>>
		<div class="footer-grad-back">
			<div class="foo-form-block">
				<div class="container">
					<div class="row justify-content-xl-start justify-content-center">
						<div class="col-xl-7 col-lg-8 col-md-10 col-12">
							<div class="row align-items-center justify-content-center">
								<?php if ($logo = opt('logo')) : ?>
									<div class="col-xl-4 col-sm-3 col-5 d-flex justify-content-center align-items-center">
										<div class="logo-foo">
											<img src="<?= $logo['url']; ?>" alt="logo">
										</div>
									</div>
								<?php endif; ?>
								<div class="col-xl-8 col-12 form-col">
									<?php if ($f_title = opt('foo_form_title')) : ?>
										<h2 class="pop-form-title"><?= $f_title; ?></h2>
									<?php endif;
									if ($f_text = opt('foo_form_subtitle')) : ?>
										<p class="pop-form-subtitle mb-4"><?= $f_text; ?></p>
									<?php endif;
									getForm('8'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container footer-container-menu">
				<div class="row justify-content-between align-items-start">
					<div class="col-lg-2 col-md col-6 foo-menu">
						<h3 class="foo-title">
							ניווט מהיר
						</h3>
						<div class="menu-border-top">
							<?php getMenu('footer-menu', '1'); ?>
						</div>
					</div>
					<div class="col-lg-2 col-md col-6 foo-menu">
						<h3 class="foo-title">
							חוגים
						</h3>
						<div class="menu-border-top">
							<?php getMenu('footer-classes-menu', '1'); ?>
						</div>
					</div>
					<div class="col-lg col-md-12 col-sm-6 col-12 foo-menu foo-menu-links">
						<h3 class="foo-title">
							<?= opt('foo_links_menu_title') ?? 'מאמרים'; ?>
						</h3>
						<div class="menu-border-top">
							<?php getMenu('footer-links-menu', '1', 'hop-hey two-columns'); ?>
						</div>
					</div>
					<div class="col-lg-auto col-sm-6 foo-menu contacts-footer-menu">
						<h3 class="foo-title">
							בואו נשאר בקשר
						</h3>
						<div class="menu-border-top">
							<?php get_template_part('views/partials/repeat', 'contact_list'); ?>
						</div>
						<?php if ($facebook) : ?>
							<div class="facebook-widget">
								<iframe src="https://www.facebook.com/plugins/page.php?href=<?= $facebook; ?>&tabs=timeline&width=300px&height=200px&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId"
										width="250px" height="150px" style="border:none;overflow:hidden" scrolling="no"
										frameborder="0" allowTransparency="true" allow="encrypted-media">
								</iframe>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>


<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
