<?php

get_header();
$fields = get_fields();
$query = get_queried_object();
$posts = get_posts([
	'numberposts' => 6,
	'post_type' => 'post',
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $query->term_id,
		]
	],
]);
$posts_all = get_posts([
	'numberposts' => -1,
	'post_type' => 'post',
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $query->term_id,
		]
	],
]);
$cats = get_terms([
	'taxonomy' => 'category',
	'hide_empty' => false,
]);
get_template_part('views/partials/content', 'top_page', [
	'title' => $query->name,
]);?>
	<article class="page-body">
		<?php get_template_part('views/partials/content', 'block_text', [
			'title' => $query->name,
			'text' => category_description(),
		]); ?>
		<div class="body-output">
			<?php if ($cats) : ?>
				<div class="container mb-4">
					<div class="row justify-content-center">
						<?php foreach ($cats as $cat) : ?>
							<div class="col-auto mb-2">
								<a class="cat-link <?= ($query->term_id === $cat->term_id) ? 'current-category' : ''; ?>"
								   href="<?= get_term_link($cat); ?>">
									<?= $cat->name; ?>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif;
			if ($posts) : ?>
				<div class="container-fluid">
					<div class="row justify-content-center">
						<div class="col-sm-11 col-12">
							<div class="row justify-content-center align-items-stretch put-here-posts">
								<?php foreach ($posts as $post) {
									get_template_part('views/partials/card', 'post_ajax', [
										'post' => $post,
									]);
								} ?>
							</div>
						</div>
					</div>
				</div>
			<?php else : ?>
				<div class="container-fluid">
					<div class="row justify-content-center">
						<div class="col-sm-11 col-12">
							<h2 class="base-title text-center">אין מאמתים בקטגוריה</h2>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
		<?php if (count($posts_all) > 6) : ?>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-auto">
						<div class="load-more-link load-more-posts" data-type="post"
							 data-tax-type="category">
							טען עוד מאמרים
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</article>
<?php get_template_part('views/partials/repeat', 'form');
if ($slider = get_field('single_slider_seo', $query)) : ?>
	<div class="transparent-slider">
		<?php get_template_part('views/partials/content', 'slider', [
			'content' => $slider,
			'img' => get_field('slider_img', $query),
		]); ?>
	</div>
<?php endif;
if ($faq = get_field('faq_item', $query)) {
	get_template_part('views/partials/content', 'faq',
		[
			'block_title' => get_field('faq_title', $query),
			'faq' => $faq,
			'img' => get_field('faq_img', $query),
		]);
}
get_footer(); ?>
